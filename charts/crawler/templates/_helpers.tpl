{{- define "crawler.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "crawler.fullname" -}}
{{- printf "%s-%s" .Release.Name .Chart.Name | replace "+" "_" | trunc 63 | trimSuffix "-"  }}
{{- end -}}

{{- define "crawler.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "crawler.labels" -}}
helm.sh/chart: {{ include "crawler.chart" . }}
{{ include "crawler.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "crawler.selectorLabels" -}}
app.kubernetes.io/name: {{ include "crawler.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "crawler.list-env-variables"}}
{{- range $key, $val := .Values.env.normal }}
- name: {{ $key }}
  value: {{ $val | quote }}
{{- end}}
{{- end }}

{{- define "crawler.mongoService" -}}
{{- printf "%s-%s" .Release.Name "mongodb" | trunc 63 | trimSuffix "-" -}}
{{- end }}

{{- define "crawler.rabbitmqService" -}}
{{- printf "%s-%s" .Release.Name "rabbitmq" | trunc 63   | trimSuffix "-" -}}
{{- end }}