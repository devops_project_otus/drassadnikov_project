Проектная работа по курсу "DevOps практики и инструменты" на тему: "Создание процесса непрерывной поставки для приложения с применением практик CI/CD и быстрой обратной связью" 
(группа "devops-2022-08")

Было использовано готовое приложение от Otus, которое включает в себя метрики, логи, тесты.
Состоит из следующих компонент:

- Search Engine Crawler
Поисковый бот для сбора текстовой информации с веб-страниц и ссылок.
Репозиторий: https://gitlab.com/devops_project_otus/search_engine_crawler

- Search Engine UI
Веб-интерфейс поиска слов и фраз на проиндексированных ботом сайтах.
Репозиторий: https://gitlab.com/devops_project_otus/search_engine_ui

Инфраструктурный репозиторий: https://gitlab.com/devops_project_otus/drassadnikov_project

# Инструменты и технологии #
- Yandex Cloud как платформа для разворачивания кластера kubernetes
- Terraform для создания кластера в облаке Yandex Cloud
- Container Registry от Gitlab для хранения образов приложения crawler
- Docker, Docker-compose для работы с образами приложений и локальной установки
- Gitlab для создания системы CI/CD
- MongoDB база для хранения данных приложения crawler
- RabbitMQ менеджер очередей для рабоыт приложения crawler
- Helm для работы с чартами приложений
- Prometheus и Grafana для системы мониторинга
- Elasticsearch, fluentd, Kibana для системы логирования

# Схема работы проекта #
Pipeline по разворачиванию инфраструктуры выполняет следующие шаги:
- c помощью Terraform происходит создание кластера kubernetes, который состоит из одной master node и двух worker node.
- происходит установка необходимых приложений для работы с кластером: yc, kubectl, helm.
- устанавливается ingress controller для балансировки и возможности подключения к кластеру из вне.
- в кластере kubernetes устанавливается приложение crawler, MongoDB, RabbitMQ из Container Registry от Gitlab с тегом latest.
- для мониторинга устанавливаются helm prometheus-operator c grafana в namespace monitirong.
- для логирования устанавливается Elasticsearch, fluentd, Kibana в namespace logging (в процессе)

Pipeline сборки и хранения выполняет следующие шаги:
- сборка приложения Search Engine Crawler или Search Engine UI в docker образ
- тестирование приложений
- отправка docker образа в Container Registry
- деплой в кластер kubernetes

# Требования к запуску проекта #
- учетная запись в Yandex Cloud
- cервисный аккаунт в Yandex Cloud с правами на создание кластера kubernetes и его сущностей (не ниже editor)
- docker, docker-compose для локальной работы с приложением
- terraform для ручного разворачивания инфраструктуры

# Как запустить проект #
Запуск Pipeline инфраструктурного репозитория и в репозиториях приложения происходит по commit.

Для ручного разворачивания инфраструктуры:
- копировать репозиторий https://gitlab.com/devops_project_otus/drassadnikov_project
- получить доступ через консоль к Yandex Cloud
- в папке terraform запустить:
    terraform init
    terraform plan
    terraform apply
- установить ingress controller:
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.47.0/deploy/static/provider/cloud/deploy.yaml
- добавить репозитории
    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm repo add stable https://charts.helm.sh/stable
- в корне репозитория запустить установку приложения crawler и его завивисостей:
    helm upgrade --install app charts/app/ -n production --create-namespace
    helm repo update
- установить приложения для мониторинга:
    helm upgrade --install stable prometheus charts/kube-prometheus-stack/ -n monitoring --create-namespace
- установить приложения для логирования (в процессе):
    helm upgrade --install prometheus charts/prometheus/ -n monitoring --create-namespace

Для проверки работы:
- для получения внешнего IP адреса выполнить команду
    kubectl get ingress -a
- в файл hosts добавить:
<IP address> rabbitmq app-ui app-grafana app-prometheus kibana
В браузере набрать адрес для получения доступа к соответствующему приложению:
  http://app-ui - поисковый бот
  http://rabbitmq - менеджер очередей
  http://app-grafana - grafana (мониторинг)
  http://app-prometheus - prometheus (мониторинг)
  http://kibana - kibana (логирование) (в процессе)

# Локальная работа с приложением #
- получить доступ к Container Registry
- копировать репозитории
    https://gitlab.com/devops_project_otus/search_engine_crawler,
	https://gitlab.com/devops_project_otus/search_engine_ui
- собрать docker образы crawler и ui при помощи соответствующих Dockerfile
- использовать docker-compose.yml для разворачивания приложения локально





