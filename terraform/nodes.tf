resource "yandex_kubernetes_node_group" "my_node_group" {
  cluster_id  = yandex_kubernetes_cluster.k8s-test-cluster.id
  name        = "k8s-nodes"
  description = "worker-nodes"
  version     = "1.21"

 depends_on = [
    yandex_kubernetes_cluster.k8s-test-cluster,
  ]

  labels = {
    "key" = "worker"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = true
      subnet_ids         = [yandex_vpc_subnet.subnet-k8-sa.id]
      /*security_group_ids = ["${yandex_vpc_security_group.my_sg.id}"]*/
    }

    resources {
      memory = 8
      cores  = 4
    }

    boot_disk {
      type = "network-hdd"
      size = 128
    }

    scheduling_policy {
      preemptible = false
    }

    metadata = {
    ssh-keys = format("%s:%s", "ubuntu", var.public_key_path)
  }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "15:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "10:00"
      duration   = "4h30m"
    }
  }
}
