/* Сервисный аккаунт  */
resource "yandex_iam_service_account" "k8s-sa-test" {
  name        = "k8s-sa-test"
  description = "service account to manage VMs"
}

resource "yandex_resourcemanager_folder_iam_binding" "k8s-editor" {
  folder_id = var.folder_id

  role = "editor"

  members = [
    "serviceAccount:${yandex_iam_service_account.k8s-sa-test.id}",
  ]
}

/* Сети */
resource "yandex_vpc_network" "network-k8s" {
  name = "network-k8s"
}

resource "yandex_vpc_subnet" "subnet-k8-sa" {
  v4_cidr_blocks = ["10.2.0.0/16"]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-k8s.id
}

/* Кластер  */
resource "yandex_kubernetes_cluster" "k8s-test-cluster" {
  name        = "k8s-test-cluster"
  description = "k8s_test_cluster_description"

  network_id = yandex_vpc_network.network-k8s.id

  master {
    version = "1.21"
    zonal {
      zone      = var.zone
      subnet_id = yandex_vpc_subnet.subnet-k8-sa.id
    }

    public_ip = true

    /*security_group_ids = ["${yandex_vpc_security_group.security_group_name.id}"]*/

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }
  }

  service_account_id      = yandex_iam_service_account.k8s-sa-test.id
  node_service_account_id = yandex_iam_service_account.k8s-sa-test.id

  labels = {
    my_key       = "my_value"
    my_other_key = "my_other_value"
  }

  release_channel = "RAPID"
  network_policy_provider = "CALICO"

}
